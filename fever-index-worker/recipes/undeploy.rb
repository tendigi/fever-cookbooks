execute "docker stop feverindexworker" do
	returns [0, 1]
end
execute "docker rm feverindexworker" do
	returns [0, 1]
end
execute "docker rmi fever:fever" do
	returns [0, 1]
end