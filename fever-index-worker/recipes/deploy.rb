execute "docker stop feverindexworker" do
  returns [0, 1]
end
execute "docker rm feverindexworker" do
  returns [0, 1]
end
execute "docker rmi fever:fever" do
  returns [0, 1]
end

docker_image 'fever' do
  action :build
  source node[:fever_docker][:dockerfile]
  notifies :redeploy, 'docker_container[feverindexworker]', :immediately
  rm true
  tag 'fever'
end

docker_container 'feverindexworker' do
  image 'fever:fever'
  container_name 'feverindexworker'
  env 'RUN_MODE=workers/index_worker.js'
  action :run
  detach true
end