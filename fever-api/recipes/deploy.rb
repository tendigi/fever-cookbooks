Chef::Log.info "Deploying"

execute "docker stop feverapi" do
	returns [0, 1]
end
execute "docker rm feverapi" do
	returns [0, 1]
end
execute "docker rmi fever:fever" do
	returns [0, 1]
end

docker_image 'fever' do
  action :build
  source node[:fever_docker][:dockerfile]
  rm true
  tag 'fever'
end

docker_container 'feverapi' do
  image 'fever:fever'
  container_name 'feverapi'
  action :run
  port "80:80"
  detach true
end