execute "docker stop feverapi" do
	returns [0, 1]
end
execute "docker rm feverapi" do
	returns [0, 1]
end
execute "docker rmi fever:fever" do
	returns [0, 1]
end