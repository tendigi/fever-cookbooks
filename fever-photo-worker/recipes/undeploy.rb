execute "docker stop feverphotoworker" do
	returns [0, 1]
end
execute "docker rm feverphotoworker" do
	returns [0, 1]
end
execute "docker rmi fever:fever" do
	returns [0, 1]
end