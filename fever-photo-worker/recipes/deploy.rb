execute "docker stop feverphotoworker" do
  returns [0, 1]
end
execute "docker rm feverphotoworker" do
  returns [0, 1]
end
execute "docker rmi fever:fever" do
  returns [0, 1]
end

docker_image 'fever' do
  action :build
  source node[:fever_docker][:dockerfile]
  notifies :redeploy, 'docker_container[feverphotoworker]', :immediately
  rm true
  tag 'fever'
end

docker_container 'feverphotoworker' do
  image 'fever:fever'
  container_name 'feverphotoworker'
  env 'RUN_MODE=workers/photo_worker.js'
  action :run
  detach true
end